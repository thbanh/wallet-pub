import { BigNumber } from "ethers";
import { PokenAddress, ChainAccess } from "../blockchain/access";
import { ChainUserPokens } from "../business/poken_business";
import { PokenContracts } from "../business/poken_constants";
import { getEtherjsAdapter } from "./ethersjs";

export const ADAPTER_ETHERSJS = "etherjs";

export type BlockchainAdapterType = typeof ADAPTER_ETHERSJS;
// | typeof WEB3JS

export interface BlockchainAdapterInterface {
  userPokens: (
    userAddr: PokenAddress,
    access: ChainAccess,
    contract: PokenContracts
  ) => Promise<ChainUserPokens>;
  formatPokens: (poken: BigNumber, decimal: number) => string;
}

export const getAdapter = (adapter: BlockchainAdapterType) => {
  const default_adapter = getEtherjsAdapter();
  if (adapter === ADAPTER_ETHERSJS) {
    return getEtherjsAdapter();
  } else {
    return default_adapter;
  }
};
