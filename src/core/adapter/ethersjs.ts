import { BigNumber, ethers } from "ethers";
import { BscProvider, BinanceProvider } from "../blockchain/bsc";
import { PokenAddress, ChainAccess } from "../blockchain/access";
import {
  InfuraProvider as PokenInfuraProvider,
  ErcProvider,
} from "../blockchain/erc";
import {
  BALANCE_OF_NFP,
  NO_PROVIDER_IDENTIFIED,
  PKN_BSC_BALANCE,
  PKN_ERC_BALANCE,
  PKN_POOLED_BY_USER,
  PKN_STACKED_BY,
  PokenContracts,
  PokenError,
  POKEN_DECIMAL,
} from "../business/poken_constants";
import * as TE from "fp-ts/TaskEither";
import * as E from "fp-ts/Either";
import { ChainUserPokens } from "../business/poken_business";
import * as O from "fp-ts/Option";
import { pipe } from "fp-ts/lib/function";
import { BlockchainAdapterInterface } from "./adapter";

export const getEtherjsAdapter = (): BlockchainAdapterInterface => {
  return {
    userPokens: userPokensEthersJsImpl,
    formatPokens: formatPokenEthersJsImpl,
  };
};

const isInfuraProvider = (
  prov: ErcProvider | BscProvider
): prov is PokenInfuraProvider => {
  return (
    (prov as PokenInfuraProvider).infuraProjectId !== undefined ||
    (prov as PokenInfuraProvider).infuraProjectSecret !== undefined
  );
};

const isBinanceProvider = (prov: BscProvider): prov is BinanceProvider => {
  return typeof (prov as BinanceProvider).binanceUrl !== undefined;
};

// Get Poken on ERC
const getPokenErcEthersJsImpl = async (
  contract: ethers.Contract,
  userAddr: PokenAddress
): Promise<E.Either<PokenError, BigNumber>> => {
  const res = await TE.tryCatch(
    () => contract.balanceOf(userAddr) as Promise<BigNumber>,
    (reason) =>
      ({ errorType: PKN_ERC_BALANCE, errorCatch: reason } as PokenError)
  )();

  return res;
};

// Get Poken on BSC
const getPokenBscEthersJsImpl = async (
  contract: ethers.Contract,
  userAddr: PokenAddress
): Promise<E.Either<PokenError, BigNumber>> => {
  const res = await TE.tryCatch(
    () => contract.balanceOf(userAddr) as Promise<BigNumber>,
    (reason) =>
      ({ errorType: PKN_BSC_BALANCE, errorCatch: reason } as PokenError)
  )();

  return res;
};

// Get the stacked tokens
const getStackedPokenEthersJsImpl = async (
  contract: ethers.Contract,
  userAddr: PokenAddress
): Promise<E.Either<PokenError, BigNumber>> => {
  const res = await TE.tryCatch(
    () => contract.pknStackedBy(userAddr) as Promise<BigNumber>,
    (reason) =>
      ({ errorType: PKN_STACKED_BY, errorCatch: reason } as PokenError)
  )();

  return res;
};

// Get the pooled tokens
const getPooledPokenEthersJsImpl = async (
  contract: ethers.Contract,
  userAddr: PokenAddress
): Promise<E.Either<PokenError, BigNumber>> => {
  const res = await TE.tryCatch(
    () => contract.pknPooledByUser(userAddr) as Promise<BigNumber>,
    (reason) =>
      ({ errorType: PKN_POOLED_BY_USER, errorCatch: reason } as PokenError)
  )();

  return res;
};

// Get the NFPs
const getNfpEthersJsImpl = async (
  contract: ethers.Contract,
  userAddr: PokenAddress
): Promise<E.Either<PokenError, BigNumber>> => {
  const res = await TE.tryCatch<PokenError, BigNumber>(
    () => contract.balanceOf(userAddr) as Promise<BigNumber>,
    (reason) =>
      ({ errorType: BALANCE_OF_NFP, errorCatch: reason } as PokenError)
  )();

  return res;
};

// Format pokens
// Poken is a 18 decimal token but NFP is 0 decimal !
const formatPokenEthersJsImpl = (poken: BigNumber, decimal = POKEN_DECIMAL) => {
  return ethers.utils
    .commify(ethers.utils.formatUnits(poken, decimal))
    .split(".")[0]
    .split(",")
    .join(" ");
};

// get all pokens
const getAllPokensEthersJsImpl = async (
  prov: ethers.providers.InfuraProvider | ethers.providers.JsonRpcProvider,
  contract: PokenContracts,
  userAddr: PokenAddress
) => {
  const pknErcContract = new ethers.Contract(
    contract.pknErc.address,
    contract.pknErc.abi,
    prov
  );

  const pknBscContract = new ethers.Contract(
    contract.pknBsc.address,
    contract.pknBsc.abi,
    prov
  );

  const stackedContract = new ethers.Contract(
    contract.stacked.address,
    contract.stacked.abi,
    prov
  );

  const pooledContract = new ethers.Contract(
    contract.pooled.address,
    contract.pooled.abi,
    prov
  );

  const nfpContract = new ethers.Contract(
    contract.nfp.address,
    contract.nfp.abi,
    prov
  );

  return {
    pknErc: await getPokenErcEthersJsImpl(pknErcContract, userAddr),
    pknBsc: await getPokenBscEthersJsImpl(pknBscContract, userAddr),
    stacked: await getStackedPokenEthersJsImpl(stackedContract, userAddr),
    pooled: await getPooledPokenEthersJsImpl(pooledContract, userAddr),
    nfp: await getNfpEthersJsImpl(nfpContract, userAddr),
  };
};

// Get the user's poken on a specific chain
const userPokensEthersJsImpl = async (
  userAddr: PokenAddress,
  access: ChainAccess,
  contract: PokenContracts
): Promise<ChainUserPokens> => {
  const provider = identifiyProviderThersJsImpl(access);

  return await pipe(
    provider,
    O.match(
      async () => ({
        pknErc: E.left({
          errorType: NO_PROVIDER_IDENTIFIED,
          errorCatch: new Error("No provider identified"),
        }),
        pknBsc: E.left({
          errorType: NO_PROVIDER_IDENTIFIED,
          errorCatch: new Error("No provider identified"),
        }),
        stacked: E.left({
          errorType: NO_PROVIDER_IDENTIFIED,
          errorCatch: new Error("No provider identified"),
        }),
        pooled: E.left({
          errorType: NO_PROVIDER_IDENTIFIED,
          errorCatch: new Error("No provider identified"),
        }),
        nfp: E.left({
          errorType: NO_PROVIDER_IDENTIFIED,
          errorCatch: new Error("No provider identified"),
        }),
      }),
      async (some) => getAllPokensEthersJsImpl(some, contract, userAddr)
    )
  );
};

const identifiyProviderThersJsImpl = (
  access: ChainAccess
): O.Option<
  ethers.providers.InfuraProvider | ethers.providers.JsonRpcProvider
> => {
  let provider = undefined;
  if (isInfuraProvider(access.provider)) {
    provider = new ethers.providers.InfuraProvider(
      access.network,
      access.provider
    );
  } else if (isBinanceProvider(access.provider)) {
    provider = new ethers.providers.JsonRpcProvider(
      access.provider.binanceUrl,
      access.network
    );
  } else {
    console.error("No detected provider");
  }

  const res = provider ? O.some(provider) : O.none;
  return res;
};
