export const BSC_NETWORK_MAINNET_CHAIN_ID = 56;
export const BSC_NETWORK_TESTNET_CHAIN_ID = 97;

export type BscNetwork =
  | typeof BSC_NETWORK_MAINNET_CHAIN_ID
  | typeof BSC_NETWORK_TESTNET_CHAIN_ID;

export type BinanceProvider = { binanceUrl: string };
export type BscProvider = BinanceProvider; // | OtherProvider
