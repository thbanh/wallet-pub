import { BscProvider, BscNetwork } from "./bsc";
import { ErcNetwork, ErcProvider } from "./erc";

export type ChainAccess = ChainAccessBsc | ChainAccessErc;
export type ChainAccessErc = { provider: ErcProvider; network: ErcNetwork };
export type ChainAccessBsc = { provider: BscProvider; network: BscNetwork };
export type PokenAddress = string; //TODO : constraint on size
