export const ERC_NETWORK_MAINNET = "mainnet";
export const ERC_NETWORK_ROPSTEN = "ropsten";
export const ERC_NETWORK_MAINNET_CHAIN_ID = 1;
export const ERC_NETWORK_ROPSTEN_CHAIN_ID = 3;

export type ErcNetwork =
  | typeof ERC_NETWORK_MAINNET
  | typeof ERC_NETWORK_ROPSTEN
  | typeof ERC_NETWORK_MAINNET_CHAIN_ID
  | typeof ERC_NETWORK_ROPSTEN_CHAIN_ID;

export type InfuraProvider = {
  infuraProjectId: string;
  infuraProjectSecret: string;
};
export type ErcProvider = InfuraProvider; //| EtherScanProvider;
